Mail transport with ansible

This is simple automation for mail routing in mail relay server using configuration management tool (Ansible) and bash.

Installation

Requirements

Linux
Python 2.7 
$ apt-get install python-pip (For some python dependency)
$ apt-get install ansible (Configuration management tool)

Usage:

To migrate mail traffic from host to host we can avoid manual task into automation with simply yaml file.

Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

#####
HELLO USER
