#!/bin/bash
transport_from_m1_to_m2(){
  echo "Transport from m1 to m2 started"
  cd /root/ansible_playbook/mail_transport
  ansible-playbook main.yml -vvvv
  echo "Transport done and back to original state"
}

echo "***********Mail migration operation list******************"
echo "*****************Enter your choice from below list******************"
echo -e "##########Transport from:#################\n1.m1 to m2"
read choice
if [ $choice == 1 ]
then
   transport_from_m1_to_m2
else
  echo "Wrong Choice"
fi
